[id]: http://www.robertjamesphillips.com/speedy/ 

# speedy

Users expect performant websites regardless of the device they are using, failure to this can have a negative impact on key metrics & conversions.

This is [an example][id] of the project after it's been built and deployed.

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
