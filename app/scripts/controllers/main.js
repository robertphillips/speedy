'use strict';

/**
 * @ngdoc function
 * @name speedyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the speedyApp
 */
angular.module('speedyApp')
  .controller('MainCtrl', function (lazyLoad, $location, $scope) {


  	$scope.isActive = function(section) {

		var location = $location.path();
		var active = location.indexOf(section) > -1;

		if(active){
			return 'current';
		}

		return false;

	};
    

  	//lazy load images  	
  	lazyLoad.init();
  	
  	var stylesCallback = function() {

		//var styles = ['http://fonts.googleapis.com/css?family=RobotoDraft:100,300,500', 'styles/main.css'];
		var styles = ['styles/main.css'];

		for(var i = 0; i < styles.length; i++){

			var l = document.createElement('link');

			l.rel = 'stylesheet';
			l.href = styles[i];
			
			var h = document.getElementsByTagName('head')[0];

			h.appendChild(l);

		}

		
	};

	if (requestAnimationFrame) {
		requestAnimationFrame(stylesCallback);
	}
	else{
		window.addEventListener('load', stylesCallback);
	}

    FastClick.attach(document.body);



    



  });
