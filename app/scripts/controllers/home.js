'use strict';

/**
 * @ngdoc function
 * @name speedyApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the speedyApp
 */
angular.module('speedyApp')
  .controller('HomeCtrl', function (lazyLoad, inView) {
    
  	// sneakily hide the duplicated content
    var pre = document.getElementById("prerender");
    pre.style.display = "none";

    //lazy load init
  	lazyLoad.init();

  	//scroll into view init
  	inView.init();




  });
