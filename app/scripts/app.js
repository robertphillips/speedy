'use strict';

/**
 * @ngdoc overview
 * @name speedyApp
 * @description
 * # speedyApp
 *
 * Main module of the application.
 */
var speedy = angular.module('speedyApp', [
	'ngAnimate',
	'ngResource',
	'ngRoute',
	'ngSanitize'
]);



speedy.config(function ($routeProvider) {
	$routeProvider
	.when('/home', {
		templateUrl: 'views/home.html',
		controller: 'HomeCtrl',
		controllerAs: 'home',
		title: 'Speedy / A Faster User Experience'
	})
	.when('/about', {
		templateUrl: 'views/about.html',
		controller: 'AboutCtrl',
		controllerAs: 'about',
		title: 'About / Speedy / A Faster User Experience'
	})
	.otherwise({
		redirectTo: '/home',
		title: 'Speedy / A Faster User Experience'
	});
});


//change Page Title based on the routers
speedy.run(['$location', '$rootScope', function($location, $rootScope) {
	$rootScope.$on('$routeChangeSuccess', function (event, current) {
		document.title = current.$$route.title;
	});
}]);