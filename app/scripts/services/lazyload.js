'use strict';

/**
 * @ngdoc service
 * @name speedyApp.lazyload
 * @description
 * # lazyload
 * Service in the speedyApp.
 */
angular.module('speedyApp')
  .service('lazyLoad', function () {


	this.init = function() { 

	  	var nodes = nodes || window.document.querySelectorAll('[data-aload]');

		if (nodes.length === undefined) {
			nodes = [nodes];
		}

		var i = 0,
			len = nodes.length,
			node;

		for (i; i < len; i += 1) {
			node = nodes[i];
			node[ node.tagName !== 'LINK' ? 'src' : 'href' ] = node.getAttribute('data-aload');
			node.removeAttribute('data-aload');
		}

	};


  });
