'use strict';

/**
 * @ngdoc service
 * @name speedyApp.inView
 * @description
 * # inView
 * Service in the speedyApp.
 */
angular.module('speedyApp')
  .service('inView', function () {


	this.init = function() { 

	  	var nodes = nodes || window.document.querySelectorAll('[data-inview]'),
	  		viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
	  		offset = 100;

		if (nodes.length === undefined) {
			nodes = [nodes];
		}


		var lastScrollY = 0,
    		ticking	= false;

		/**
		* Callback for our scroll event - just
		* keeps track of the last scroll value
		*/
		function onScroll() {
			lastScrollY = (window.scrollY || window.pageYOffset);
			requestTick();
		}


		/**
		* Calls rAF if it's not already
		* been done already
		*/
		function requestTick() {
			if(!ticking) {
				if (requestAnimationFrame) {
					requestAnimationFrame(update);
				}
				else {
					update();
				}
				ticking = true;
			}
		}


		/**
		* Our animation callback
		*/
		function update() {


			for (var i = 0; i < nodes.length; i++) {

				var node = nodes[i];

				if((lastScrollY + viewportHeight - offset) >= node.offsetTop){
					
					var newClass = node.getAttribute('data-inview');

					if(newClass !== null){
						node.className += " " + newClass;
						node.removeAttribute('data-inview');
					}
					
					//updateList(i);

				}

				// 
				
			}

			// allow further rAFs to be called
			ticking = false;
		}


		// function updateList(index) {


		// 	nodes.splice(index, 1);

		// }

		window.addEventListener('scroll', onScroll, false);
		
		if (requestAnimationFrame) {
			requestAnimationFrame(update);
		}


	};


  });
